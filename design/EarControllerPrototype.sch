EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Ear Controller Prototype"
Date "2018-11-07"
Rev "0.1"
Comp "Feliform Labs"
Comment1 "Licensed under the Solderpad Hardware License, version 2.0"
Comment2 "Copyright 2018 Feliform Labs"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3575 1525 750  175 
U 5BD0DB90
F0 "AudioFilter0" 50
F1 "AudioFilterPrototype.sch" 50
F2 "AMPLITUDE" O R 4325 1600 50 
F3 "MIC" I L 3575 1600 50 
$EndSheet
$Sheet
S 5075 1225 1850 1350
U 5BD1C0BC
F0 "Microcontroller" 50
F1 "MicrocontrollerPrototype.sch" 50
F2 "AMPLITUDE0" I L 5075 1600 50 
F3 "PWM1" O R 6925 1400 50 
F4 "PWM2" O R 6925 1500 50 
F5 "PWM3" O R 6925 1600 50 
F6 "PWM0" O R 6925 1300 50 
F7 "CIC_TX" O R 6925 1775 50 
F8 "CIC_RX" I R 6925 1875 50 
$EndSheet
$Comp
L power:+6V #PWR0118
U 1 1 5BD25EF2
P 9825 1100
F 0 "#PWR0118" H 9825 950 50  0001 C CNN
F 1 "+6V" H 9840 1273 50  0000 C CNN
F 2 "" H 9825 1100 50  0001 C CNN
F 3 "" H 9825 1100 50  0001 C CNN
	1    9825 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5BD423CD
P 8125 3650
F 0 "#PWR0117" H 8125 3400 50  0001 C CNN
F 1 "GND" H 8130 3477 50  0000 C CNN
F 2 "" H 8125 3650 50  0001 C CNN
F 3 "" H 8125 3650 50  0001 C CNN
	1    8125 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8125 3650 8125 3550
Wire Wire Line
	8125 3550 8675 3550
$Comp
L power:+12V #PWR0119
U 1 1 5BD463EA
P 9475 3425
F 0 "#PWR0119" H 9475 3275 50  0001 C CNN
F 1 "+12V" H 9490 3598 50  0000 C CNN
F 2 "" H 9475 3425 50  0001 C CNN
F 3 "" H 9475 3425 50  0001 C CNN
	1    9475 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9475 3425 9475 3550
Wire Wire Line
	9475 3550 9250 3550
Wire Wire Line
	8750 3650 8675 3650
Wire Wire Line
	8675 3650 8675 3550
Connection ~ 8675 3550
Wire Wire Line
	8675 3550 8750 3550
Wire Wire Line
	9250 3650 9475 3650
Wire Wire Line
	9475 3650 9475 3550
Connection ~ 9475 3550
Text Label 9575 3750 2    50   ~ 0
CIC_TX
Text Label 8425 3750 0    50   ~ 0
CIC_RX
Text Label 7250 1775 2    50   ~ 0
CIC_TX
Text Label 7250 1875 2    50   ~ 0
CIC_RX
Wire Wire Line
	7250 1875 6925 1875
Wire Wire Line
	6925 1775 7250 1775
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J103
U 1 1 5BD6A6C6
P 8950 3650
F 0 "J103" H 9000 3967 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 9000 3876 50  0000 C CNN
F 2 "EarControllerParts:TSW-104-07-T-T" H 8950 3650 50  0001 C CNN
F 3 "~" H 8950 3650 50  0001 C CNN
	1    8950 3650
	1    0    0    -1  
$EndComp
$Sheet
S 5250 3425 1600 700 
U 5BD7D14E
F0 "Power" 50
F1 "PowerPrototype.sch" 50
$EndSheet
Text Notes 10525 2075 0    50   ~ 0
Servo\nconnector
Text Notes 8400 3350 0    50   ~ 0
Component interface\nconnector
Wire Wire Line
	4325 1600 5075 1600
$Comp
L Device:Microphone_Condenser MK101
U 1 1 5BD56EE5
P 2425 1900
F 0 "MK101" H 2555 1946 50  0000 L CNN
F 1 "Microphone_Condenser" H 2555 1855 50  0000 L CNN
F 2 "EarControllerParts:CMC-6015-47P" V 2425 2000 50  0001 C CNN
F 3 "~" V 2425 2000 50  0001 C CNN
	1    2425 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2425 1700 2425 1600
Wire Wire Line
	2425 1600 3575 1600
$Comp
L power:GNDA #PWR0101
U 1 1 5BD59931
P 2425 2100
F 0 "#PWR0101" H 2425 1850 50  0001 C CNN
F 1 "GNDA" H 2430 1927 50  0000 C CNN
F 2 "" H 2425 2100 50  0001 C CNN
F 3 "" H 2425 2100 50  0001 C CNN
	1    2425 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10075 1750 9925 1750
Wire Wire Line
	10075 1650 9825 1650
Wire Wire Line
	9925 1450 9925 1750
Wire Wire Line
	6925 1500 7925 1500
Wire Wire Line
	6925 1400 7500 1400
Wire Wire Line
	9925 1450 10075 1450
Wire Wire Line
	9825 1350 9825 1650
Connection ~ 9825 1350
Wire Wire Line
	10075 1350 9825 1350
Connection ~ 9825 1650
Wire Wire Line
	9925 2400 9925 2350
$Comp
L power:GNDD #PWR0200
U 1 1 5BD8D60A
P 9925 2400
F 0 "#PWR0200" H 9925 2150 50  0001 C CNN
F 1 "GNDD" H 9929 2245 50  0000 C CNN
F 2 "" H 9925 2400 50  0001 C CNN
F 3 "" H 9925 2400 50  0001 C CNN
	1    9925 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10075 2350 9925 2350
Connection ~ 9925 2350
Wire Wire Line
	9925 2050 9925 2350
Wire Wire Line
	10075 2050 9925 2050
Connection ~ 9925 2050
Connection ~ 9925 1750
Wire Wire Line
	9925 1750 9925 2050
Wire Wire Line
	9825 1650 9825 1950
Wire Wire Line
	9825 1950 9825 2250
Connection ~ 9825 1950
Wire Wire Line
	10075 1950 9825 1950
Wire Wire Line
	10075 2250 9825 2250
$Comp
L EarControllerParts:Connector_03x08 J102
U 1 1 5BD25FD7
P 10275 1800
F 0 "J102" H 10453 1841 50  0000 L CNN
F 1 "Connector_03x04" H 10453 1750 50  0000 L CNN
F 2 "EarControllerParts:TSW-104-07-T-T" H 10275 1150 50  0001 C CNN
F 3 "" H 10275 1150 50  0001 C CNN
	1    10275 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 3750 9575 3750
Wire Wire Line
	8425 3750 8750 3750
Wire Wire Line
	6925 1300 7050 1300
Wire Wire Line
	9825 1100 9825 1350
$Comp
L Connector:TestPoint TP105
U 1 1 5BD45192
P 10150 3125
F 0 "TP105" H 10208 3245 50  0000 L CNN
F 1 "TestPoint" H 10208 3154 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 10350 3125 50  0001 C CNN
F 3 "~" H 10350 3125 50  0001 C CNN
	1    10150 3125
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP106
U 1 1 5BD45226
P 10625 3125
F 0 "TP106" H 10683 3245 50  0000 L CNN
F 1 "TestPoint" H 10683 3154 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 10825 3125 50  0001 C CNN
F 3 "~" H 10825 3125 50  0001 C CNN
	1    10625 3125
	1    0    0    -1  
$EndComp
Text Label 10450 3275 2    50   ~ 0
CIC_RX
Text Label 10950 3275 2    50   ~ 0
CIC_TX
Wire Wire Line
	10150 3125 10150 3275
Wire Wire Line
	10150 3275 10450 3275
Wire Wire Line
	10625 3125 10625 3275
Wire Wire Line
	10625 3275 10950 3275
$Comp
L Connector:TestPoint TP102
U 1 1 5BD464A8
P 7500 1025
F 0 "TP102" H 7558 1145 50  0000 L CNN
F 1 "TestPoint" H 7558 1054 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 7700 1025 50  0001 C CNN
F 3 "~" H 7700 1025 50  0001 C CNN
	1    7500 1025
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP103
U 1 1 5BD465D4
P 7925 1025
F 0 "TP103" H 7983 1145 50  0000 L CNN
F 1 "TestPoint" H 7983 1054 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 8125 1025 50  0001 C CNN
F 3 "~" H 8125 1025 50  0001 C CNN
	1    7925 1025
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP104
U 1 1 5BD46612
P 8350 1025
F 0 "TP104" H 8408 1145 50  0000 L CNN
F 1 "TestPoint" H 8408 1054 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 8550 1025 50  0001 C CNN
F 3 "~" H 8550 1025 50  0001 C CNN
	1    8350 1025
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP101
U 1 1 5BD4665A
P 7050 1025
F 0 "TP101" H 7108 1145 50  0000 L CNN
F 1 "TestPoint" H 7108 1054 50  0000 L CNN
F 2 "EarControllerParts:TestPoint-1206" H 7250 1025 50  0001 C CNN
F 3 "~" H 7250 1025 50  0001 C CNN
	1    7050 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1025 7050 1300
Wire Wire Line
	7500 1025 7500 1400
Connection ~ 7500 1400
Wire Wire Line
	7925 1025 7925 1500
Connection ~ 7925 1500
Wire Wire Line
	8350 1025 8350 1600
Wire Wire Line
	6925 1600 8350 1600
Text Label 9500 2150 0    50   ~ 0
PWM0
Text Label 9500 1850 0    50   ~ 0
PWM1
Text Label 9500 1550 0    50   ~ 0
PWM2
Text Label 9525 1250 0    50   ~ 0
PWM3
Wire Wire Line
	9525 1250 10075 1250
Wire Wire Line
	9500 1550 10075 1550
Wire Wire Line
	10075 1850 9500 1850
Wire Wire Line
	9500 2150 10075 2150
Text Label 8600 1600 2    50   ~ 0
PWM3
Text Label 8600 1500 2    50   ~ 0
PWM2
Text Label 8600 1400 2    50   ~ 0
PWM1
Text Label 8600 1300 2    50   ~ 0
PWM0
Wire Wire Line
	8350 1600 8600 1600
Connection ~ 8350 1600
Wire Wire Line
	7925 1500 8600 1500
Wire Wire Line
	7500 1400 8600 1400
Wire Wire Line
	7050 1300 8600 1300
Connection ~ 7050 1300
$EndSCHEMATC
