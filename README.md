# Ear Controller Prototype

This board design is a prototype ear controller for the [Feliform Labs
ear control research area](https://feliformlabs.com/research/ear-control).

## Directory structure

The `design` folder contains the current design files and bill of materials
in editable formats. The `release` folder contains printable bills of materials,
fabrication files, and assembly drawings.

## File formats

All files in this repository can be edited with standard open-source tools.
Design files are in KiCAD formats, and bills of materials are in OpenDocument
formats.

## License

Copyright 2018 Feliform Labs

Files in this repository are licensed under the Solderpad Hardware License,
Version 2.0 ([LICENSE-Solderpad.txt](LICENSE-Solderpad.txt)) or, at your option,
the Apache License, Version 2.0 ([LICENSE-Apache.txt](LICENSE-Apache.txt)).
