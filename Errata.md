# Ear Controller Prototype Errata and Testing Notes

## Version 0.1

### 2018-12-01

#### Power

As expected, 6 V supply from switching regulator is somewhat noisy. The 3.3 V
supplies are less noisy.

With the microcontroller running and the LED on about half the time, total
power consumption on the 12 V supply is about 2 mA.

#### Microcontroller

##### Reset

After loading new code onto the microcontroller, it will not correctly run
the new code until the board power supply is turned off and on again.
This could be caused by the debug adapter not being able to fully reset the
microcontroller. The capacitor between reset and ground could be causing a
problem. This warrants investigation with an oscilloscope.

##### Firmware

Serial output looks good, although I don't have an adapter to fully test it
with.

PWM output is not working. The signals are always low. This should be fixable
with trial and error.

ADC readings are around 1000, varying within a range of about 100. This makes
sense overall, based on looking at the amplitude signal.

#### Analog

The output from the microphone has no discernible signal. The preamplifer output
has no discernible signal. However, the amplitude output does do something. By
tapping on the microphone, I can cause a jump in the amplitude signal.

I might just need to increase the preamplifer gain. According to the
[sound detector documentation](https://learn.sparkfun.com/tutorials/sound-detector-hookup-guide/configuration),
a resistor of 100 kiloohms to a few megaohms may work.
